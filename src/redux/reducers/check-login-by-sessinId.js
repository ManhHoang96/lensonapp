import { CHECK_LOGIN_BY_SESSIONID } from '../actions/types';
const initialState = {
    data: false,
}
export default function checkLoginBySessionId(state = initialState, action) {
    switch (action.type) {
        case CHECK_LOGIN_BY_SESSIONID:
            return {
                ...state,
                data: action.data,
            }
        default:
            return state
    }
}