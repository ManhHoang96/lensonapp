import {GET_CURRENT_USER} from './types';

export function getCurrentUser(data){
    return{
        type: GET_CURRENT_USER,
        data
    }
}