import { FETCH_CUSTOMER, FETCH_CUSTOMER_SUCCESS, FETCH_CUSTOMER_FAILURE } from './types';

export function getCustomer() {
    return {
        type: FETCH_CUSTOMER
    }
}
export function getCustomerSuccess(data) {
    return {
        type: FETCH_CUSTOMER_SUCCESS,
        data
    }
}

export function getCustomerFailure() {
    return {
        type: FETCH_CUSTOMER_FAILURE
    }
}

export function fetchCustomer(url) {
    return (dispatch) => {
        dispatch(getCustomer())
        fetch(url)
            .then(data => data.json())
            .then(json => {
                dispatch(getCustomerSuccess(json.data))
            })
            .catch(err => {
                dispatch(getCustomerFailure());
            })
    }
}
export function fetchCustomer2(url, startTime, endTime, employeeId, status, cb) {
    return (dispatch) => {
        dispatch(getCustomer())
        fetch(url, {

            method: 'POST',
            body: JSON.stringify({
                startTime: startTime,
                endTime: endTime,
                employeeId: employeeId,
                status: status
            }),
            headers: { "Content-Type": "application/json; charset=utf-8" }
        })
            .then(data => data.json())
            .then(json => {
                dispatch(getCustomerSuccess(json.data))
            })
            .catch(err => {
                dispatch(getCustomerFailure());
            })
    }
} 