import React, { Component } from 'react';
import { View, Dimensions, TouchableOpacity } from 'react-native';
import { Text } from 'native-base';

class CardTransaction extends Component {
     state = {  }
     render() {
          return (
               <TouchableOpacity 
                    style={[styles.container, {backgroundColor: this.props.color}]}
                    onPress={this.props.gotoDetail}
               >
               </TouchableOpacity>
          );
     }
}

const screen = Dimensions.get('window')
const styles = {
     container: {
          width: screen.width - 20,
          height: 120,
          marginTop: 10
     }
}
export default CardTransaction;