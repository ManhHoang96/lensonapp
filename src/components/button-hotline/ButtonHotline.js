//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import color from '../../ultis/styles/common-css';
import Communications from 'react-native-communications';
// create a component
export default class ButtonHotLine extends Component {
    constructor(props) {
        super(props);
        phoneNum = '1800 66 44';
    }
    render() {
        return (
            <TouchableOpacity
                style={[styles.container, { ...this.props.style }]}
                onPress={() => Communications.phonecall(phoneNum, true)}
            >
                <Text style={styles.text}>Hotline : </Text>
                <Text style={styles.phoneNum}>{phoneNum}</Text>
            </TouchableOpacity>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        height: 50,
        width: '90%',
        flexDirection: 'row',
        backgroundColor: color.colorBlue,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 5,
        alignSelf: 'center'
    },
    phoneNum: {
        color: '#ffff00',
        fontSize: 20
    },
    text: {
        color: 'white',
        fontSize: 20
    }
});

