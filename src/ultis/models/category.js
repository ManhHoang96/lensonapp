import Realm from 'realm';

class Category extends Realm.Object {
     // Write some method, function in here....
}

Category.schema = {
     name: 'category',
     properties: {
          id: {
               type: 'string',
          },
          name:{
               type: 'string',
          },
          description: {
               type: 'string',
          },
          picture: {
               type: 'string',
          },
          childNum: {
               type: 'int',
               default: 0
          },
          code: {
               type: 'string',
          },
          status: {
               type: 'number',
               default: 1
          },
          lastUpdate: {
               type: 'number',
               default: 0
          }
     }

};

export default new Realm({ schema: [Category]});

