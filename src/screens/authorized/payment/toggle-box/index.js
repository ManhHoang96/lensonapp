import React from 'react';
import PropTypes from 'prop-types'
import { Text, View, Image, TouchableWithoutFeedback, Animated } from 'react-native'

// external libs
import Icon from 'react-native-vector-icons/MaterialIcons'

// styles
import styles from './styles';
import color from '../../../../ultis/styles/common-css';
class ToggleBox extends React.Component {

  static propTypes = {
    arrowColor: PropTypes.string,
    arrowSize: PropTypes.number,
    arrowDownType: PropTypes.string,
    arrowUpType: PropTypes.string,
    children: PropTypes.element.isRequired,
    expanded: PropTypes.bool,
    label: PropTypes.string.isRequired,
    style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }

  static defaultProps = {
    arrowColor: 'rgb(178, 178, 178)',
    arrowSize: 30,
    arrowDownType: 'keyboard-arrow-down',
    arrowUpType: 'keyboard-arrow-up',
    expanded: false,
    style: {},
    value: null,
  }

  constructor(props) {
    super(props)

    this.icons = {
      'up': this.props.arrowUpType,
      'down': this.props.arrowDownType,
    }

    this.state = {
      expanded: this.props.expanded,
    }
  }

  toggle = () => {
    let initialValue = this.state.expanded ? this.state.maxHeight + this.state.minHeight : this.state.minHeight
    let finalValue = this.state.expanded ? this.state.minHeight : this.state.minHeight + this.state.maxHeight

    this.setState({
      expanded: !this.state.expanded
    })

    this.state.animation.setValue(initialValue)
    Animated.spring(
      this.state.animation,
      {
        toValue: finalValue,
        bounciness: 0,
      }
    ).start()
  }

  setMaxHeight = (event) => {
    if (!this.state.maxHeight) {
      this.setState({
        maxHeight: event.nativeEvent.layout.height
      })
    }
  }

  setMinHeight = (event) => {
    if (!this.state.animation) {
      this.setState({
        animation:
        this.state.expanded ?
          new Animated.Value() :
          new Animated.Value(parseInt(event.nativeEvent.layout.height))
      })
    }
    this.setState({
      minHeight: event.nativeEvent.layout.height
    })
  }

  render() {
    const icon = this.icons[this.state.expanded ? 'up' : 'down']

    return (
      <Animated.View style={[styles.box, this.props.style, { height: this.state.animation }]}>
        <TouchableWithoutFeedback
          onPress={this.toggle}
          onLayout={this.setMinHeight}
        >
          <View>
            <View style={styles.titleContainer}>
              <Text style={styles.label}>{this.props.label}</Text>
              <Text style={{ color: color.colorBlue }}>Đã thanh toán</Text>

            </View>
            <View style={styles.titleContainer}>
              <Text>Người trả :  </Text>
              <Text style={{ color: 'red' }}>admin</Text>
            </View>
            <View style={styles.titleContainer}>
              <Text>Số tiền:</Text>
              <Text style={{ color: 'red' }}>{this.props.moneyPaid}VND</Text>
            </View>
            <View style={styles.titleContainer}>
              <Text>Ngày trả:</Text>
              <Text style={{ color: 'red' }}>{this.props.lastUpdate}</Text>
            </View>
            <View style={{ justifyContent: 'space-between', alignItems: 'center', flexDirection: 'row', width: '100%' }}>
              {this.props.value ? <Text style={styles.value}>{this.props.value}</Text> : null}
              <Icon
                name={icon}
                color={this.props.arrowColor}
                // style={styles.buttonImage}
                size={this.props.arrowSize}
              />
              <View style={{ width: '35%' }}></View>
            </View>
          </View>
        </TouchableWithoutFeedback>
        <View style={styles.body} onLayout={this.setMaxHeight} aaa={'Kien'}>
          {this.props.children}
        </View>

      </Animated.View>
    )
  }
}
export default ToggleBox