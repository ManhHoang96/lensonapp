import React, { Component } from 'react';
import { View } from 'react-native';
import { Container, Content, Tab, Tabs } from 'native-base';
import PaymentHistory from './pay-history-tab';
import Revenue from './revenue-tab';
import Header from '../../../components/header/Header';
import color from '../../../ultis/styles/common-css';
class Payment extends Component {
    setTitleHeader(page) {
        switch (page) {
            case 0:
                this.Header.setTitle('Thanh toán');
                break;
            case 1:
                this.Header.setTitle('Lịch sử thanh toán');
                break;
        }
    }
    render() {
        
        return (
            <View style={{ flex: 1 }}>
                <Header
                    navigation={this.props.navigation}
                    title={'Thanh toán'}
                       
                />
                <Tabs
                    initialPage={0}
                    tabBarPosition={'bottom'}
                    tabBarUnderlineStyle={{ height: 0 }}
                    locked={true}
                   
                >
                    <Tab
                        heading="Doanh số"
                        activeTabStyle={styles.activeTabStyle}
                        activeTextStyle={styles.activeTextStyle}
                        tabStyle={styles.tabStyle}
                        textStyle={styles.textStyle}
                        ref={(ref) => this.Tab = ref}
                    >
                        <Revenue
                            navigation={this.props.navigation}
                            
                        />
                    </Tab>
                    <Tab
                        heading="Lịch sử thanh toán"
                        activeTabStyle={styles.activeTabStyle}
                        activeTextStyle={styles.activeTextStyle}
                        tabStyle={styles.tabStyle}
                        textStyle={styles.textStyle}
                    >

                        <PaymentHistory
                            navigation={this.props.navigation}
                        />
                    </Tab>

                </Tabs>
            </View>
        );
    }
}
const styles = {
    activeTabStyle: {
        backgroundColor: color.colorBlue
    },
    activeTextStyle: {
        color: color.colorWhite,
        fontWeight: 'normal'
    },
    tabStyle: {
        borderTopWidth: 1,
        borderTopColor: color.colorBlue,
        backgroundColor: 'white'
    },
    textStyle: {
        color: color.colorBlue,
        fontWeight: 'normal'
    }
}

export default Payment;
