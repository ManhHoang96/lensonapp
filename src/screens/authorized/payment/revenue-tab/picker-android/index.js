import React, { Component } from "react";
import { Platform } from "react-native";
import { Container, Header, Title, Content, Button, Icon, Text, Right, Body, Left, Picker, Form, Item as FormItem } from "native-base";
const Item = Picker.Item;
export default class PickerExample extends Component {
     constructor(props) {
          super(props);
          this.state = {
               selected1: "key1"
          };
     }
     onValueChange(value) {
          this.setState({
               selected1: value
          });
            this.props.getAddressCity(value);
     }
     render() {
          return (

               <Form>
                    <Picker
                         iosHeader="Select one"
                         mode="dropdown"
                         style={this.props.style}
                         textStyle={{color: '#fff'}}
                         selectedValue={this.state.selected1}
                         onValueChange={this.onValueChange.bind(this)}
                    >
                    {
                         this.props.data.map((e,i) => (
                              <Item key={i} label={e.name} value={e.id} />
                         ))
                    }
                    </Picker>
               </Form>

          );
     }
}