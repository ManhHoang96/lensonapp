
import CryptoJS from 'crypto-js';
import md5 from '../../../../libs/md5/md5';
import HanderTextInput from '../../../../ultis/controller-global/HanderTextInput';
import { Toast } from 'native-base';
export default class AccountInfoController {

  titleHeader = 'Đăng ký tài khoản';
  account = '';
  password = '';
  email = '';
  retypePassword = '';
  handerTextInput = new HanderTextInput();
  isDisable = false;
  setIsDisable(isDisable) {
    this.isDisable = isDisable;
  }
  getIsDisable() {
    return this.isDisable;
  }
  setAccount(account) {
    this.account = account;
  }
  getAccount() {
    return this.account;
  }
  setPassword(password) {
    this.password = password;
  }
  getPassword() {
    return password;
  }
  setEmail(email) {
    this.email = email;
  }
  getEmail() {
    return email;
  }
  setRetypePassword(retypePassword) {
    this.retypePassword = retypePassword;
  }
  getRetypePassword() {
    return retypePassword;
  }

  getTitleHeader() {
    return this.titleHeader;
  }
  showToast(text) {
    Toast.show({
      text: text,
      position: 'top',
      buttonText: 'Okay',
      duration: 2000
    })
  }

  convertAccount(value) {
    let array = [
      'à', 'á', 'ạ', 'ả', 'ã', 'â', 'ầ', 'ấ', 'ậ', 'ẩ', 'ẫ', 'ă', 'ằ', 'ắ', 'ặ', 'ẳ', 'ẵ',
      'è', 'é', 'ẹ', 'ẻ', 'ẽ', 'ê', 'ề', 'ế', 'ệ', 'ể', 'ễ',
      'ì', 'í', 'ị', 'ỉ', 'ĩ',
      'ò', 'ó', 'ọ', 'ỏ', 'õ', 'ô', 'ồ', 'ố', 'ộ', 'ổ', 'ỗ', 'ơ', 'ờ', 'ớ', 'ợ', 'ở', 'ỡ',
      'ù', 'ú', 'ụ', 'ủ', 'ũ', 'ư', 'ừ', 'ứ', 'ự', 'ử', 'ữ',
      'ỳ', 'ý', 'ỵ', 'ỷ', 'ỹ',
      'đ',
      '!', '@', '%', '\^', '\*', '\(|\)', '\+|\=', '\<', '\>', '\?', '\/', '\.', '\:', '\;', ' ', '\&', '\#', '\[', '\]', '~', '$', '_',
      '-', '+', '$'
    ]
    for (let i = 0; i < array.length; i++) {
      if (value.includes(array[i])) return false;
    }
    return true;
  }
  validateEmail(email) {
    var re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email.toLowerCase());
  }

  checkValueAccount(navigation, parent) {
    if (this.isDisable === false) {
      this.isDisable = true;
      if (this.account === '' || this.account === undefined) {
        this.showToast('Tài khoản trống!');
        this.isDisable = false;
        return;
      }
      if (this.convertAccount(this.account) === false) {
        this.showToast('Tài khoản sai,chứa ký tự đặc biệt!');
        this.isDisable = false;
        return;
      }

      if (this.email === '' || this.email === undefined) {
        this.showToast('Email trống');
        this.isDisable = false;
        return;
      }
      if (!this.validateEmail(this.email)) {
        this.showToast('Email sai!');
        this.isDisable = false;
        return;
      }
      if (this.password === '' || this.password === undefined) {
        this.showToast('Mật khẩu trống!');
        this.isDisable = false;
        return;
      }
      if (this.retypePassword === '' || this.retypePassword === undefined) {
        this.showToast('Nhập lại mật khẩu!');
        this.isDisable = false;
        return;
      }
      if (this.password !== this.retypePassword) {
        this.showToast('Mật khẩu không khớp!');
        this.isDisable = false;
        return;
      }
      if (parent.props.isOnline === true) {
        parent.setState({
          isLoading: true,
        })
        fetch(`https://app-dot-casper-electric.appspot.com/api?action=getUserInfoByAccount&account=${this.account}`)
          .then((response) => response.json())
          .then((responseJson) => {
            parent.setState({
              isLoading: false,
            })
            if (responseJson.data.length === 0) {
              navigation.navigate('UserInfo', {
                account: this.account,
                email: this.email,
                password: this.encryptorPass(this.account, this.password).toString(),
              });
              this.isDisable = false;
            } else {
              this.showToast('Tài khoản đã tồn tại');
              this.isDisable = false;
            }

          })
          .catch((error) => {
            console.error(error);
          });
      } else {
        this.showToast('Không có kết nối internet!');
        this.isDisable = false;
      }
    }
  }

  encryptorPass(userName, password) {
    let value = md5(userName + '.' + password)
    let keyHex = CryptoJS.enc.Utf8.parse(value);
    var encrypted = CryptoJS.DES.encrypt(password, keyHex, {
      mode: CryptoJS.mode.ECB,
      padding: CryptoJS.pad.Pkcs7
    });
    return encrypted
  }
}
