//import liraries
import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, AsyncStorage, TouchableOpacity, ActivityIndicator, NetInfo, Platform } from 'react-native';
import { NavigationActions } from 'react-navigation';
import { connect } from 'react-redux';
import { getCurrentUser } from '../../../redux/actions/get-current-user';
import { getOnline } from '../../../redux/actions/get-online';
class Splash extends Component {
    constructor(props) {
        super(props);
        isRun = true;
    }

    componentWillMount() {
        if (Platform.OS === 'android') {
            NetInfo.getConnectionInfo().then(
                (connectionInfo) => {
                    this.props.getOnline((connectionInfo.type === 'none') ? false : true);
                    if (isRun === true) {
                        isRun = false;
                        this.onStart(connectionInfo)
                    }
                });
            NetInfo.addEventListener(
                'connectionChange',
                (connectionInfo) => {
                    this.props.getOnline((connectionInfo.type === 'none') ? false : true);
                }
            )
        } else {
            NetInfo.addEventListener(
                'connectionChange',
                (connectionInfo) => {
                    this.props.getOnline((connectionInfo.type === 'none') ? false : true);
                    if (isRun === true) {
                        isRun = false;
                        this.onStart(connectionInfo)
                    }
                }
            )

        }
    }
    onStart = async (connectionInfo) => {
        try {
            const uuid = await AsyncStorage.getItem('key');
            const account = await AsyncStorage.getItem('account'); 
            if (uuid === null) {
                this.login('Login')
            }
            else if (connectionInfo.type === 'none') {
                this.login('Home');
                this.props.getCurrentUser(responseJson.data.account);
            }
            else {
                fetch('https://app-dot-casper-electric.appspot.com/api?action=loginFromPreviousSession', {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                        sessionId: uuid,
                    })
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        if (responseJson.data.length === 0) {
                            //data empty
                            this.login('Login')
                        }
                        else {
                            //set data user to redux
                            this.props.getCurrentUser(responseJson.data.account);
                            this.login('Home')
                        }
                    })
                    .catch((error) => {
                    });
            }

        } catch (error) {
        }
    }
    login(nameScreen) {
        const resetAction = NavigationActions.reset({
            index: 0,
            actions: [
                NavigationActions.navigate({ routeName: nameScreen })
            ]
        })
        this.props.navigation.dispatch(resetAction)
    }


    render() {
        return (
            <View style={styles.container}>
                <Image
                    source={require('../../../../img/logo.png')}
                    style={styles.imageLogo}
                    resizeMode={'contain'}
                />
                <ActivityIndicator

                />
            </View>
        );
    }
}

// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    imageLogo: {
        width: '90%',
        height: '40%'
    }
});
const mapStateToProps = (state, ownProps) => {
    return {
        isOnline: state.getOnline.data
    }
}
const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        getCurrentUser: (data) => {
            dispatch(getCurrentUser(data))
        },
        getOnline: (data) => {
            dispatch(getOnline(data))
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Splash);

